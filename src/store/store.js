import {configureStore, getDefaultMiddleware} from "@reduxjs/toolkit";
import {rootReducer} from "../reducers/rootReducer";

const middleware = [
    ...getDefaultMiddleware()
]

export const store = configureStore({
    reducer:rootReducer,
    middleware:middleware
})

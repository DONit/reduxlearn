import {createAction, createSlice} from "@reduxjs/toolkit";

const initState = {
    user: null,
    isRegistered: false
}

const userSlice = createSlice({
    name: 'user',
    initialState: initState,
    reducers: {
        registerUser: (state, action) => {
            state.user = action.payload
            state.isRegistered = true
        },
        unregisterUser: state => {
            state.user = null
            state.isRegistered = false
        }
    }
})

export const userReducer = userSlice.reducer
export const {registerUser, unregisterUser} = userSlice.actions

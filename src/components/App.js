import React, {Component, Fragment} from "react"
import Registration from "./Registration";
import Profile from "./Profile";

class App extends Component {
    render() {
        return (
            <div className="mt-5 d-flex justify-content-center">
                <div className="col-4">
                    <Registration/>
                    <Profile/>
                </div>
            </div>
        )
    }
}

export default App

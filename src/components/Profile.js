import React, {Component} from "react"
import {connect} from "react-redux";
import {Button} from "react-bootstrap";
import {unregisterUser} from "../reducers/userReducer";

class Profile extends Component {
    handleOnClick = (e) => {
        this.props.unregisterUser()
    }
    render() {
        const {user, isRegistered} = this.props
        if (!isRegistered) return (<div className="alert alert-danger">Вы не зарегистрированы!</div>)
        return (
            <div className="d-flex justify-content-center">
                <div>
                    <p>Имя пользователя: {user.username}</p>
                    <p>E-Mail: {user.email}</p>
                    <Button variant={"secondary"} onClick={this.handleOnClick}>Разрегистрироваться</Button>
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    user: state.userReducer.user,
    isRegistered: state.userReducer.isRegistered
})

export default connect(mapStateToProps, {unregisterUser})(Profile)

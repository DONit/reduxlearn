import React, {Component, Fragment} from "react"
import {Button, Form} from "react-bootstrap";
import {connect} from "react-redux";
import {registerUser} from "../reducers/userReducer";

class Registration extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                email: "",
                username: "",
            }
        }
    }


    handleOnChange = (e) => {
        const {name, value} = e.target
        const {user} = this.state
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        })
    }

    handleOnSubmit = (e) => {
        e.preventDefault()
        this.props.registerUser(this.state.user)
    }

    render() {
        if(this.props.isRegistered) return null
        return (
            <div className="d-flex justify-content-center mb-5">
                <div className="card shadow-lg w-100">
                    <div className="card-header text-center shadow-sm">Регистрация</div>
                    <Form className="card-body" onSubmit={this.handleOnSubmit}>
                        <Form.Group controlId="formBasicUsername">
                            <Form.Label>Имя пользователя:</Form.Label>
                            <Form.Control onChange={this.handleOnChange} name="username" type="text"
                                          placeholder="Введите имя пользователя"/>
                        </Form.Group>
                        <Form.Group controlId="formBasicEMail">
                            <Form.Label>E-Mail:</Form.Label>
                            <Form.Control onChange={this.handleOnChange} name="email" type="email"
                                          placeholder="Введите E-Mail"/>
                        </Form.Group>
                        <div className="d-flex justify-content-end">
                            <Button className="mt-2" variant="success" type="submit">
                                Зарегистрироваться
                            </Button>
                        </div>
                    </Form>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    isRegistered: state.userReducer.isRegistered
})

export default connect(mapStateToProps, {registerUser})(Registration)
